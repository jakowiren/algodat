from heapq import heappop, heappush
import sys
from collections import defaultdict
import random

import time
start = time.time()

N, M = list(map(int, sys.stdin.readline().split(' ')))  # reading number of people and pairs

friends = []
for i in range(M):  # reading descriptions
    edge_description = (sys.stdin.readline().replace('\n','').split(' '))
    friends.append(list(map(int, edge_description)))
input_time = time.time()-start

# Creating an undirected graph as a dictionary with values as a list of tuples. 
# for eg. {1: [(2, 3)], 2: [(1, 3), (3, 7)]}, with the second element in tuples being the weight. (ie. 1 going to 2 and vice versa, with weight 3).
start = time.time()

def make_graph(friends):
    graph = defaultdict(list)

    for pair in friends:
        u, v, w = pair[0], pair[1], pair[2]
        v_weight = (v,w)
        u_weight = (u,w)

        #if u not in graph: # not needed with deafultdict
           # graph[u] = []
        graph[u].append(v_weight)

        #if v not in graph:  # not needed with deafultdict
           # graph[v] = []
        graph[v].append(u_weight)

    return graph

def prim(graph):
    root = random.choice(list(graph)) # Random root
    totalWeight = 0                   
    explored = set()            
    not_explored = [(0, root)]   # Not yet explored edges ordered by weight
    while len(not_explored) > 0: 
        weight, closestFriend = heappop(not_explored) # popping the edge with the lowest weight O(log n)
        if closestFriend not in explored:         
            explored.add(closestFriend)  #Adding closest node to already explored set. And then increasing total weight.       
            totalWeight += weight
            for friend, weight in graph[closestFriend]: #For each node in graph, pushing the not yet explored friends to the heap.
                if friend not in explored:
                    heappush(not_explored, (weight, friend))  # O(log n)
    print(totalWeight)


graph = make_graph(friends) #Creating undirected graph from input
make_graph_time = time.time()-start
start = time.time()

prim(graph)
prim_time = time.time()-start

sys.stderr.write("input time   :  "+ str(input_time))
sys.stderr.write("graph time:  "+ str(make_graph_time))
sys.stderr.write("prim time:  "+ str(prim_time))
sys.stderr.write("Total" + str(input_time+ make_graph_time+prim_time))

# time bash check_solution.sh python3 makingfriends.py
