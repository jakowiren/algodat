import time
import sys
import queue



class Node:

    def __init__(self, word, neighbours, visited, pred):
        self.word = word
        self.neighbours = neighbours
        self.visited = False
        self.pred = pred

TO_PRIME = dict( \
    a=2, b=3, c=5, d=7, e=11, \
    f=13, g=17, h=19, i=23, j=29, \
    k=31, l=37, m=41, n=43, o=47, \
    p=53, q=59, r=61, s=67, t=71, \
    u=73, v=79, w=83, x=89, y=97, z=101 \
    )

start = time.time()

N, Q = list(map(int, sys.stdin.readline().split(' ')))  # reading number of words and queries
words = [0]*N
worddict = {}
for i in range(N):  # reading five-letter words
    words[i] = sys.stdin.readline().replace('\n','')



queries = []
for i in range(Q):  # reading queries
    queries.append(sys.stdin.readline().replace('\n','').split(' '))

read_input_time = time.time()-start
start = time.time()

def make_nodes(words):
    for word in words:
        worddict[word] = Node(word, [],False, 0)
    return worddict

def comparenodes(node1, node2):
  
    substringprimeproduct1 = 1
    substringprimeproduct2=1
    substring1 = node1[1:]
    substring2 = node2
    for char in substring1:
        substringprimeproduct1 = TO_PRIME.get(char)*substringprimeproduct1 #create unique prime product for 4 last letters
    for char in substring2:
        substringprimeproduct2 = TO_PRIME.get(char)*substringprimeproduct2
    if ((substringprimeproduct2/substringprimeproduct1) in TO_PRIME.values()):
        return True
    else: return False


def make_graph(words):
    graph = make_nodes(words)
    for current in graph.items():
        for temp in graph.items():
            if comparenodes(current[0], temp[0]) and current != temp:
                current[1].neighbours.append(temp[1])
    return graph




    
def countPath(node):
    count = 0
    while node.pred != 0:
        node = node.pred
        count += 1
    return count


def BFS(G, startNode, endNode):
    #startNode.visited = True
    for n in graph.items():
        n[1].visited = False
        n[1].pred = 0
    if startNode.word == endNode.word:
        print(0)
        return
    
    q = queue.Queue(maxsize=N)
    q.put(startNode)
    startNode.visited = True
    while q.qsize() > 0:
        v = q.get() #queue has 0(1) rather than O(n)
        for adjacent in v.neighbours:
            if adjacent.visited==False:
                adjacent.visited = True
                q.put(adjacent)
                adjacent.pred = v
                if adjacent.word == endNode.word:
                   # print ('found path s - t')
                    print (countPath(adjacent))
                    return
    print('Impossible')

   
graph = make_graph(words)

make_graph_time = time.time()-start
start = time.time()



for query in queries:
    BFS(graph, graph[query[0]], graph[query[1]])
BFS_time = time.time()-start


sys.stderr.write(" Read input time: "+ str(read_input_time))
sys.stderr.write(" Make graph time: "+ str(make_graph_time))
sys.stderr.write(" BFS Time : "+ str(BFS_time))
sys.stderr.write(" Total" + str(read_input_time+make_graph_time + BFS_time))


